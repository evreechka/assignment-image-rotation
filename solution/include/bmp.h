#ifndef IMAGE
#define IMAGE
#include "image.h"
#endif
#include <stdio.h>
struct bmp_header;
enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
};
enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_WRONG
};
enum write_status to_bmp(FILE *file, struct image const *img);
enum read_status from_bmp(FILE *file, struct image* img);
