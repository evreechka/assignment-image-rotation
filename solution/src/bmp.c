#include "../include/bmp.h"
#include "stdbool.h"
#include <stdint.h>
struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

static uint32_t get_padding(struct bmp_header header) {
    return (header.biWidth * 3) % 4;
}

static bool read_image(FILE *file, struct bmp_header header, struct image *img) {
    *img = init_image(header.biWidth, header.biHeight);
    for (int i = 0; i < img->height; i++) {
            if (!fread(img->data + i * img->width, sizeof(struct pixel) * img->width, 1, file))
                return false;
        fseek(file, get_padding(header) ? 4 - (header.biWidth * 3) % 4 : 0, SEEK_CUR);
    }
    return true;
}

enum read_status from_bmp(FILE *file, struct image *img) {
    struct bmp_header header = {0};
    if (!fread(&header, sizeof(struct bmp_header), 1, file))
        return READ_INVALID_HEADER;
    if (header.bfType != 0x4D42)
        return READ_INVALID_SIGNATURE;
    if (header.biBitCount < 24)
        return READ_INVALID_BITS;
    if (!read_image(file, header, img))
        return READ_WRONG;
    return READ_OK;
}

static uint32_t get_bmp_file_size(struct image const *img) {
    return sizeof(struct bmp_header)
           + 3 * sizeof(uint8_t) * (img->height * img->width)
           + ((img->width % 4) ? (img->height + 1) * (4 - (img->width % 4)) : 0);
}

static uint32_t get_bmp_image_size(struct bmp_header const header) {
    return header.bfileSize - sizeof(struct bmp_header);
}

static struct bmp_header create_bmp_header(struct image const *img) {
    struct bmp_header header = {0};
    header.bfType = ('M' << 8) + 'B';
    header.bOffBits = 54;
    header.biSize = 40;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = 1;
    header.biBitCount = 24;
    header.bfileSize = get_bmp_file_size(img);
    header.biSizeImage = get_bmp_image_size(header);
    return header;
}

enum write_status to_bmp(FILE *file, struct image const *img) {
    struct bmp_header header = create_bmp_header(img);
    if (!fwrite(&header, sizeof(struct bmp_header), 1, file)) {
        return WRITE_ERROR;
    }
    for (size_t i = 0; i < img->height; i++) {
        if (!fwrite(img->data + i * img->width, sizeof(struct pixel) * img->width, 1, file)) {
            return WRITE_ERROR;
        }
        uint32_t padding = get_padding(header);
        if (padding != 0) {
            char buff[4] = {0};
            if (!fwrite(buff, 4 - padding, 1, file))
                return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}

