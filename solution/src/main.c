#include "../include/rotate.h"
#include "../include/bmp.h"

int main(int argc, char **argv) {
    if (argc != 3) {
        fprintf(stderr, "Write in this format: ./image-transformer <source-image> <transformed-image>");
        return 1;
    }
    FILE *in = fopen(argv[1], "rb");
    FILE *out = fopen(argv[2], "wb");
    struct image new_image = {0};
    struct image image = {0};
    if (in) {
        if (from_bmp(in, &image) == READ_OK) {
            rotate_90_img(&image, &new_image);
        }
        free_data(image);
        fclose(in);
    }

    if (out) {
        if (to_bmp(out, &new_image) == WRITE_ERROR) {
            fprintf(stderr, "Some problems to write new picture to the file.");
            return 1;
        }
        free_data(new_image);
        fclose(out);
    }

    return 0;
}

