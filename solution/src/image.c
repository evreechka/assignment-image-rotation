#include "stdlib.h"
#include <stdint.h>
struct __attribute__((packed)) pixel {
    uint8_t b, g, r;
};
struct image {
    uint64_t width, height;
    struct pixel * data;
};
struct image init_image(uint64_t width, uint64_t height) {
    struct pixel * data = malloc(sizeof(struct pixel) * height * width);
    return (struct image) {.width = width, .height = height, .data = data};
}
void free_data(struct image image) {
    free(image.data);
}
