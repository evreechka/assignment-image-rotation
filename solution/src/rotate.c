#include "../include/image.h"
#include <stdint.h>
#include <stdlib.h>
static struct pixel* get_pixel_by_coordinates(struct image const* img, size_t x, size_t y) {
    return &(img->data[img->width * x + y]);
}

void rotate_90_img(struct image const* img, struct image * rotate_img) {
    uint64_t width = img->width;
    uint64_t height = img->height;
    *rotate_img = init_image(height, width);
    for (size_t i = 0; i < height; i++) {
        for (size_t j = 0; j < width; j++) {
            size_t new_i = j;
            size_t new_j = img->height - i - 1;
            rotate_img->data[rotate_img->width * new_i + new_j] = *get_pixel_by_coordinates(img, i, j);
        }
    }
}

